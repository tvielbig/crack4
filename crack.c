#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"
#include "entry.h"

const int PASS_LEN=20;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file rainbow_file\n", argv[0]);
        exit(1);
    }

    // DONE: Open the text hashes file for reading
    FILE *hf = fopen("hashes.txt", "r");
    // DONE: Check to make sure file was successfully opened
    if(!hf) printf("can't open %s for reading\n", "hashes.txt");

    // DONE: Open the binary rainbow file for reading
    FILE *bf = fopen("rainbow.bow", "rb");
    // DONE: Check to make sure if was successfully opened
    if(!bf) printf("Can't open %s for reading\n", "rainbow.bow");

    // DONE:: This is a long list of things to do, so
    //       break it up into functions!
    char hash[HASH_LEN];
    
    // For each hash (read one line at a time out of the file):
    struct entry *e = (struct entry *) malloc(sizeof(struct entry));
    while(fscanf(hf, "%s\n", hash) != EOF)
    {
    //   Convert hex string to digest (use hex2digest. See md5.h)
        unsigned char *dig = hex2digest(hash);
    //   Extract the first three bytes
        unsigned char idx[3];
        idx[0] = dig[0];
        idx[1] = dig[1];
        idx[2] = dig[2];
    //   Use those three bytes as a 24-bit number
        int index = (idx[0]*256*256 + idx[1]*256 + idx[2])*sizeof(struct entry);
    //   Seek to that location in the rainbow file
        fseek(bf, index, SEEK_SET);
    //   Read the entry found there
        fread(e, sizeof(struct entry), 1, bf);
    
        char *new_hash = digest2hex(e->hash);         
        while(memcmp(hash, new_hash, sizeof(dig)) != 0) 
        {
            new_hash = digest2hex(e->hash);
            fseek(bf, sizeof(struct entry), SEEK_CUR+sizeof(struct entry));
            fread(e, sizeof(struct entry), 1, bf);
        }
    //  Display the hash and the plaintext
        printf("Hash: %s   Pass: %s\n", new_hash, e->pass);
    }
    free(e);
}


/*
+Open the hashes file for reading.
+Open the rainbow file for reading in binary mode.
+Read the hashes, line by line, from the hashes file.
+For each hash, do the following:
+Convert it into a raw digest
+Extract the first three bytes and convert it into an int
+Calculate from this number the position in the file where the corresponding entry will be found
+Use fseek to move the file pointer to this location
+Use fread to read the entry found at this position
Compare the digest in the entry to the digest you are trying to crack
If they match, print out the corresponding password (from the entry)
If they do not match, the entry you're looking for is probably in the next slot. Use fread again to get the next entry. Compare again. Keep reading until you find the matching entry.
When done, fclose all the files.
*/
        
